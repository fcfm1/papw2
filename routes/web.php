<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    //return view('welcome');
    return view('main');
});

Route::resource('trainers', 'TrainerController');

Route::get('trainers/{trainer}/pokemons', 'PokemonController@index');
Route::post('trainers/{trainer}/pokemons', 'PokemonController@store');

Route::get('trainers/{trainer}/videos', 'VideoController@index');
Route::post('trainers/{trainer}/videos', 'VideoController@store');

Route::get('trainers/{trainer}/followers', 'FollowerController@index');
Route::post('trainers/{trainer}/followers', 'FollowerController@store');

Route::resource('videos', 'VideoController');

Route::get('videos/{video}/comments', 'CommentController@index');
Route::post('videos/{video}/comments', 'CommentController@store');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

/*Route::get('/', function () {
    return view('main');
});*/
