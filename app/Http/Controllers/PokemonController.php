<?php

namespace App\Http\Controllers;

use App\Pokemon;
use App\User;
use Illuminate\Http\Request;

class PokemonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $trainer, Request $request)
    {
        if($request->ajax()){
            return response()->json($trainer->pokemons, 200);
        }
        return view('pokemons.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $trainer, Request $request)
    {
        if($request->ajax()){

            if($request->hasFile('picture')){
                $file = $request->file('picture');
                $name = time().$file->getClientOriginalName();
                $file->move(public_path().'/images/', $name);
            }
            $pokemon = new Pokemon();
            $pokemon->name = $request->input('name');
            $pokemon->picture = $name;
            $pokemon->user()->associate($trainer)->save();

            return response()->json([
                "message" => "Pokemon creado correctamente.",
                "pokemon" => $pokemon
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
