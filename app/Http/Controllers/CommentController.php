<?php

namespace App\Http\Controllers;

use App\Comment;
use App\User;
use App\Video;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Video $video, Request $request)
    {
        if($request->ajax()){
            return response()->json($video->comments, 200);
        }
        return view('comments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Video $video, Request $request)
    {
        if($request->ajax()){

            $comment = new Comment();
            $comment->comment = $request->input('comment');
            //$comment->video()->associate($video)->save();
            //$trainer = User::where('slug', '=', $request->session()->get('slug'))->firstOrFail();
            //$trainer = User::where('slug', '=', 'ChazyChaz');
            $trainer = $request->user();
            $comment->user()->associate($trainer);
            $comment->video()->associate($video)->save();

            return response()->json([
                "message" => "comment creado correctamente.",
                "comment" => $comment
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
