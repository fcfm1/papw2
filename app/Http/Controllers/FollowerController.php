<?php

namespace App\Http\Controllers;

use App\Follower;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FollowerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $trainer, Request $request)
    {
        /*if($request->ajax()){
            return response()->json($trainer->followers, 200);
        }*/

        //$followers_ids = $trainer->followers;

        $followers_ids = $trainer->followers->map(function ($item, $key) {
            return $item->user_id;
        });

        $followers = DB::table('users')->select('name', 'avatar')->whereIn('id', $followers_ids)->get();


        if($request->ajax()){
            return response()->json($followers, 200);
        }

        return view('followers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $trainer, Request $request)
    {
        /*$follower = $request->user();

        DB::table('follows')->insert(
            [
                'user_id' => $trainer->id,
                'follower_id' => $follower->id
            ]
        );*/

        if($request->ajax()) {

            $follower = new Follower();
            $follower->user_id = $request->user()->id;
            $follower->save();

            $follower_user = Follower::where('user_id', $follower->user_id)->first();

            $trainer->followers()->attach($follower_user);

            return response()->json([
                "message" => "Follower creado correctamente.",
                "follower" => $follower
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
