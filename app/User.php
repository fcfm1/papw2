<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'slug', 'avatar', 'cover',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function pokemons(){
        return $this->hasMany('App\Pokemon');
    }

    public function videos(){
        return $this->hasMany('App\Video');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    /*public function follows(){
        return $this->hasMany('App\Follow');
    }*/

    public function followers(){
        return $this->belongsToMany('App\Follower');
    }
}
