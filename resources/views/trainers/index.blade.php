@extends('layouts.app')

@section('title', 'Trainers')

@section('content')
    <div class="row">
        @foreach ($trainers as $trainer)
            <div class="col-sm" style="width: 18rem; border-radius:10px">
                <div class="card text-center" style="width: 18rem; border-radius:10px">
                    <div style="background-color:rgba(255, 0, 0, 0.8); border-radius:10px">
                        <img src="/images/{{$trainer->avatar}}" class="card-img-top rounded-circle mx-auto d-block" alt="...">
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">{{$trainer->name}}</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="/trainers/{{$trainer->slug}}" class="btn btn-primary" style="background-color:rgba(0, 0, 0, 0.5); border: none;">Perfil</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
