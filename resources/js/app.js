/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('spinner-component', require('./components/widgets/SpinnerComponent.vue').default);

Vue.component('add-pokemon-component', require('./components/pokemons/AddPokemonComponent.vue').default);
Vue.component('create-pokemon-component', require('./components/pokemons/CreatePokemonComponent.vue').default);
Vue.component('pokemon-list-component', require('./components/pokemons/PokemonListComponent.vue').default);

Vue.component('add-video-component', require('./components/videos/AddVideoComponent.vue').default);
Vue.component('post-video-component', require('./components/videos/PostVideoComponent.vue').default);
Vue.component('video-list-component', require('./components/videos/VideoListComponent.vue').default);

Vue.component('add-comment-component', require('./components/comments/AddCommentComponent.vue').default);
Vue.component('post-comment-component', require('./components/comments/PostCommentComponent.vue').default);
Vue.component('comment-list-component', require('./components/comments/CommentListComponent.vue').default);

//Vue.component('follower-List-component', require('./components/followers/FollowerListComponent.vue').default);
Vue.component('follower-list-component', require('./components/followers/FollowerListComponent.vue').default);
Vue.component('follow-trainer-component', require('./components/followers/FollowTrainerComponent.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
